gitless (0.8.8-7) unstable; urgency=medium

  * Team Upload
  * Remove runtime dependency on python3-pkg-resources (Closes: #1083419)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 03 Feb 2025 00:04:25 +0100

gitless (0.8.8-6) unstable; urgency=medium

  * Fix FTBFS with recent versions of python3-sh. Closes: #1082144
  * Use debputy's X-Style: black.
  * Add the year 2025 to my debian/* copyright notice.

 -- Peter Pentchev <roam@debian.org>  Wed, 29 Jan 2025 22:32:19 +0200

gitless (0.8.8-5) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.0 with no changes.
  * Drop the version of the python3-sh build dependency.
  * Explicitly add dh-sequence-single-binary as a build dependency.
  * autopkgtest scripts: fix some issues reported by shellcheck.
  * Declare compliance with Policy 4.7.0 with no changes.
  * Bump the debhelper compat level to 14 and let debhelper add some
    default dependencies automatically.
  * Include dpkg's default.mk for completeness.
  * Declare dpkg-build-api v1, drop the implied Rules-Requires-Root: no.
  * Add the years 2021 and 2024 to my debian/* copyright notice.

 -- Peter Pentchev <roam@debian.org>  Wed, 03 Jul 2024 20:06:09 +0300

gitless (0.8.8-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Peter Pentchev ]
  * Drop dh-python, dh-sequence-python3 is enough.

 -- Peter Pentchev <roam@debian.org>  Mon, 26 Oct 2020 11:40:12 +0200

gitless (0.8.8-3) unstable; urgency=medium

  * Drop the adequate autopkgtest.
  * Drop the Name and Contact upstream metadata fields.
  * Use dh-sequence-python3 as a build dependency.
  * Use debian/clean to remove some directories.
  * Update to debhelper compat level 13:
    - drop the conditional around the dh_auto_test target
    - `dh_missing --fail-missing` is the default now

 -- Peter Pentchev <roam@debian.org>  Sun, 10 May 2020 17:24:30 +0300

gitless (0.8.8-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Peter Pentchev ]
  * Uncap the pygit2 requirement: gitless works with 1.x. Closes: #947597
  * Add the year 2020 to my debian/* copyright notice.
  * Declare compliance with Debian Policy 4.5.0 with no changes.

 -- Peter Pentchev <roam@debian.org>  Sun, 09 Feb 2020 16:37:49 +0200

gitless (0.8.8-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.0.
  * d/control: Fix wrong Vcs-*.

  [ Peter Pentchev ]
  * Mark the adequate autopkgtest as superficial.
  * Use the test-name autopkgtest feature.
  * Annotate test-related build dependencies with <!nocheck>.
  * Run the Salsa CI jobs.
  * New upstream version:
    - drop the path-none and typos patch, integrated upstream
    - add the relax-pygit2-dep patch to relax the version requirement
  * Export LANGUAGE=C during the tests to fix a FTBFS.
  * Correct the years placement in the copyright file.

 -- Peter Pentchev <roam@debian.org>  Sat, 28 Sep 2019 19:30:04 +0300

gitless (0.8.6-4) unstable; urgency=medium

  * Move to the Debian Python Applications Packaging Team.
  * Add a git-buildpackage config file.
  * Switch the homepage URL to the HTTPS scheme.
  * Add the year 2019 to my debian/* copyright notice.
  * Always run the upstream test suite.

 -- Peter Pentchev <roam@debian.org>  Wed, 06 Feb 2019 15:24:41 +0200

gitless (0.8.6-3) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.3.0 with no changes.
  * Drop the Lintian override related to B-D: debhelper-compat -
    Lintian 2.5.98 no longer emits that error.
  * Bump the debhelper compatibility level to 12 with no changes and
    run the dh_missing tool with the --fail-missing option.

 -- Peter Pentchev <roam@debian.org>  Wed, 26 Dec 2018 01:19:46 +0200

gitless (0.8.6-2) unstable; urgency=medium

  * Add the path-none patch to catch up with pygit2's new behavior of
    returning None instead of raising a KeyError when not in a Git
    repository.  Closes: #907146
  * Declare compliance with Debian Policy 4.2.0 with no changes.
  * Change the "MIT" license name to "Expat" in the copyright file to
    avoid ambiguity with the multiple versions of MIT-like licenses.
  * Add the typos patch to correct two typographical errors in
    the upstream source and also propagate the fix to the debian/gl.1
    manual page.
  * Add a trivial autopkgtest running adequate on the binary package.
  * Use the B-D: debhelper-compat (= 11) mechanism.

 -- Peter Pentchev <roam@debian.org>  Sat, 25 Aug 2018 21:59:29 +0300

gitless (0.8.6-1) unstable; urgency=low

  * Initial release.  Closes: #839644

 -- Peter Pentchev <roam@debian.org>  Wed, 16 May 2018 00:59:12 +0300
