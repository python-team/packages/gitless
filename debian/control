Source: gitless
Section: devel
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Peter Pentchev <roam@debian.org>,
Build-Depends:
 debhelper (>> 13.15.2~),
 dh-sequence-python3,
 dh-sequence-single-binary,
 dpkg-build-api (= 1),
 git <!nocheck>,
 python3-all,
 python3-clint <!nocheck>,
 python3-pygit2 <!nocheck>,
 python3-setuptools,
 python3-sh <!nocheck>,
Standards-Version: 4.7.0
X-DH-Compat: 14
Homepage: https://gitless.com/
Vcs-Git: https://salsa.debian.org/python-team/packages/gitless.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/gitless
X-Style: black

Package: gitless
Architecture: all
Depends:
 git,
 ${python3:Depends},
Recommends:
 less,
Multi-Arch: foreign
Description: version control system on top of Git
 Gitless is an experimental version control system built on top of Git.
 Many people complain that Git is hard to use.  The Gitless authors think
 the problem lies deeper than the user interface, in the concepts
 underlying Git.  Gitless is an experiment to see what happens if
 a simple veneer is put on an app that changes the underlying concepts.
 Because Gitless is implemented on top of Git (could be considered what
 Git pros call a "porcelain" of Git), you can always fall back on Git.
 And of course your coworkers you share a repo with need never know that
 you're not a Git aficionado.
