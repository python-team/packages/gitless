#!/bin/sh

set -e

pwd="$(pwd)"

testf="$pwd/debian/test-home/.gitconfig"
if [ ! -f "$testf" ]; then
	echo "No '$testf' file; are you in the Debian package source directory?" 1>&2
	exit 1
fi

HOME="$pwd/debian/test-home"
LC_ALL='C.UTF-8'
LANGUAGE='C'
PYTHON3="$(py3versions -d)"
export HOME LC_ALL LANGUAGE PYTHON3

dir="$(mktemp -d -t gitless-test.XXXXXX)"
# shellcheck disable=SC2064
trap "rm -rf -- '$dir'" EXIT HUP INT QUIT TERM

echo "Using directory $dir"
./debian/test-gl-simple "$dir"
